/**
 * @file blitpack.h
 * @brief Blitpack implementation for C99.
 * @copyright Copyright 2022 Peter Gebauer (MIT license)
 *
 * @section write_functions Write functions
 *
 * TODO: describe write functions
 *
 * @section read_functions Read functions
 *
 * TODO: describe read functions
 *
 * @section license License
 *
 * ```unparsed
 * Copyright 2022 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ```
 */
#ifndef BLITPACK_H
#define BLITPACK_H

#include <limits.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#define BLITPACK_API

#if defined(__GNUC__) || defined(__clang__)
#define BLITPACK_PRINTF(format, varargs) __attribute__ ((__format__ (__printf__, format, varargs)))
#else
#define BLITPACK_PRINTF(format, varargs)
#endif

/**
 * @brief Head.
 */
#define BLITPACK_HEAD 0x424c4954

/**
 * @brief Head swapped endian.
 */
#define BLITPACK_HEAD_SWAPPED 0x54494c42

/**
 * @brief Maximum size of any object.
 */
#define BLITPACK_SIZE_MAX 4294967292

/**
 * @def BLITPACK_ZEROPAD
 * @brief If defined to 1 at build time all padding bytes to achieve 4 byte
 * alignment will be zeroed, otherwise they will be left uninitialized.
 * @note This does not include the single zero terminator for strings.
 */
#ifndef BLITPACK_ZEROPAD
#define BLITPACK_ZEROPAD 0
#endif

/**
 * @brief The operation will overrun the buffer size.
 */
#define BLITPACK_EOVERRUN -3

/**
 * @brief The operation will cause an integer to overflow.
 */
#define BLITPACK_EOVERFLOW -4

/**
 * @brief The packet is invalid.
 */
#define BLITPACK_EPACKET -5

/**
 * @brief The endian is wrong.
 */
#define BLITPACK_EENDIAN -6

/**
 * @brief The iterator has reached the end.
 */
#define BLITPACK_EITERATOR -7

/**
 * @brief An argument contains an invalid value.
 */
#define BLITPACK_EINVAL -8

/**
 * @brief Invalid type encountered.
 */
#define BLITPACK_ETYPE -9

/**
 * @brief The type could not be converted.
 */
#define BLITPACK_ECONV -10

/**
 * @brief The read offset is invalid.
 */
#define BLITPACK_EOFFSET -11

/**
 * @brief A value in the packet is invalid.
 */
#define BLITPACK_EVALUE -12

/**
 * @brief A value in the packet has a misaligned size.
 */
#define BLITPACK_EALIGN -13

/**
 * @brief A max size @ref BLITPACK_SIZE_MAX has been reached.
 */
#define BLITPACK_ESIZE -14

/**
 * @brief NIL.
 */
#define BLITPACK_TNIL 'N'

/**
 * @brief Infinity.
 */
#define BLITPACK_TINF 'Q'

/**
 * @brief A boolean.
 */
#define BLITPACK_TBOOL 'x'

/**
 * @brief Signed 8 bit integer.
 */
#define BLITPACK_TINT8 'b'

/**
 * @brief Unsigned 8 bit integer.
 */
#define BLITPACK_TUINT8 'B'

/**
 * @brief Signed 16 bit integer.
 */
#define BLITPACK_TINT16 's'

/**
 * @brief Unsigned 16 bit integer.
 */
#define BLITPACK_TUINT16 'S'

/**
 * @brief `int32_t`.
 */
#define BLITPACK_TINT32 'i'

/**
 * @brief `uint32_t`.
 */
#define BLITPACK_TUINT32 'I'

/**
 * @brief `int64_t`.
 */
#define BLITPACK_TINT64 'l'

/**
 * @brief `uint64_t`.
 */
#define BLITPACK_TUINT64 'L'

/**
 * @brief `float`.
 */
#define BLITPACK_TFLOAT 'f'

/**
 * @brief `double`.
 */
#define BLITPACK_TDOUBLE 'd'

/**
 * @brief A zero terminated array of unsigned 8 bit integers.
 */
#define BLITPACK_TSTRING 'c'

/**
 * @brief A packet.
 */
#define BLITPACK_TPACKET 'p'

/**
 * @brief A key string value pair map packet.
 */
#define BLITPACK_TMAP 'm'

#define BLITPACK_ISALIGNED(v) ((((v) & 3)) == 0)
#define BLITPACK_ISALIGNEDW(v, sz) ((((v) & ((sz) - 1))) == 0)
#define BLITPACK_ALIGNED(v) ((((v) + 3) >> 2) << 2)

// struct blitpack_value
// {
//     bool vbool;
//     int8_t vint8;
//     uint8_t vuint8;
//     int8_t vint8;
//     uint8_t vuint8;
// };

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Get an error string from an error code.
 * @param error The error code.
 * @returns A pointer to the error message, never NULL.
 */
BLITPACK_API const char *blitpack_strerr(
    int64_t error
);

/**
 * @brief Initialize a new packet.
 * @param buffer If non-NULL store packet bytes here.
 * @param buffer_size Store at most this many bytes in @p buffer.
 * @param length The number of members (values) the packet will have.
 * @returns The number of stored bytes on success or a negative error
 * code on failure. If @p buffer is NULL the number of required bytes
 * is returned instead.
 */
BLITPACK_API int64_t blitpack_init(
    void *buffer,
    int64_t buffer_size,
    uint32_t length
);

/**
 * @brief Write value to the packet.
 * @param[out] buffer If non-NULL store the value in this packet.
 * @param buffer_size Store at most this many bytes into @p buffer.
 * @param[in, out] iterator Keep track of where in the packet we are,
 * ignored if @p buffer is NULL. Must be initialized to zero before first use.
 * @returns The number of additional bytes used to store the
 * value or a negative error code on failure.
 * @see See section @ref write_functions for details.
 */
BLITPACK_API int64_t blitpack_write_nil(
    void *buffer,
    int64_t buffer_size,
    uint32_t *iterator
);

/**
 * @brief Write value to the packet.
 * @param[out] buffer If non-NULL store the value in this packet.
 * @param buffer_size Store at most this many bytes into @p buffer.
 * @param[in, out] iterator Keep track of where in the packet we are,
 * ignored if @p buffer is NULL. Must be initialized to zero before first use.
 * @returns The number of additional bytes used to store the
 * value or a negative error code on failure.
 * @see See section @ref write_functions for details.
 */
BLITPACK_API int64_t blitpack_write_inf(
    void *buffer,
    int64_t buffer_size,
    uint32_t *iterator
);

/**
 * @brief Write value to the packet.
 * @param[out] buffer If non-NULL store the value in this packet.
 * @param buffer_size Store at most this many bytes into @p buffer.
 * @param[in, out] iterator Keep track of where in the packet we are,
 * ignored if @p buffer is NULL. Must be initialized to zero before first use.
 * @returns The number of additional bytes used to store the
 * value or a negative error code on failure.
 * @param value If non-NULL @p length members will be copied to
 * the packet, if NULL the packet value memory will be zeroed instead.
 * @param length The number of members to copy from @p value.
 * @returns The number of additional bytes used to store the
 * value or a negative error code on failure.
 * @see See section @ref write_functions for details.
 */
BLITPACK_API int64_t blitpack_write_bool(
    void *buffer,
    int64_t buffer_size,
    uint32_t *iterator,
    const bool *value,
    uint32_t length
);

/**
 * @brief Write value to the packet.
 * @param[out] buffer If non-NULL store the value in this packet.
 * @param buffer_size Store at most this many bytes into @p buffer.
 * @param[in, out] iterator Keep track of where in the packet we are,
 * ignored if @p buffer is NULL. Must be initialized to zero before first use.
 * @returns The number of additional bytes used to store the
 * value or a negative error code on failure.
 * @param value If non-NULL @p length members will be copied to
 * the packet, if NULL the packet value memory will be zeroed instead.
 * @param length The number of members to copy from @p value.
 * @returns The number of additional bytes used to store the
 * value or a negative error code on failure.
 * @see See section @ref write_functions for details.
 */
BLITPACK_API int64_t blitpack_write_key_bool(
    void *buffer,
    int64_t buffer_size,
    uint32_t *iterator,
    const char *key,
    uint32_t key_n,
    const bool *value,
    uint32_t length
);

#ifdef __cplusplus
}
#endif

#endif
