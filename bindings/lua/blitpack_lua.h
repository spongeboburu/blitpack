#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "blitpack.h"

struct blitpack_lua_buffer
{
    int64_t size;
    unsigned char buffer[];
};

BLITPACK_API int blitpack_lua_write(
    lua_State *L,
    const void *buffer,
    int64_t buffer_size
);

BLITPACK_API int blitpack_lua_read(
    lua_State *L,
    void *buffer,
    int64_t buffer_size
);


BLITPACK_API int luaopen_blitpack(
    lua_State *L
);
