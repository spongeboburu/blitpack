#include "blitpack_lua.h"

#include <limits.h>

inline static size_t blitpack_lua_table_len_(lua_State *L, int index)
{
    int ret = 0;
#if defined(LUA_VERSION__) && LUA_VERSION__ < 53
#else
    lua_len(L, index);
    ret = lua_tointeger(L, -1);
    lua_pop(L, 1);
#endif
    return ret;
}

static int blitpack_lua_buffer_length_(
    lua_State *L
)
{
    struct blitpack_lua_buffer *userdata = luaL_checkudata(L, 1, "buffer");
    lua_pushinteger(L, userdata->size);
    return 1;
}

static int blitpack_lua_buffer_write_(
    lua_State *L
)
{
    struct blitpack_lua_buffer *userdata = luaL_checkudata(L, 1, "buffer");
    int64_t size = 0;
    if (lua_gettop(L) == 3)
    {
        const char *typetag = luaL_checkstring(L, 2);
        luaL_checktype(L, 3, LUA_TTABLE);
        size_t typetag_size = blitpack_lua_table_len_(L, -2);
        size_t table_size = blitpack_lua_table_len_(L, -1);
        if (typetag_size > BLITPACK_SIZE_MAX - sizeof(uint32_t[3]) || typetag_size > INT_MAX)
            return luaL_error(L, "Typetag too long");
        if (table_size > BLITPACK_SIZE_MAX - sizeof(uint32_t[3]) || table_size > INT_MAX)
            return luaL_error(L, "Values table too long");
        // if (table_size != typetag_size)
        //     return luaL_error(L, "Typetag length (%d) differs from the number of table members (%d)", (int)typetag_size, table_size);
        int64_t ret = blitpack_init(userdata->buffer, userdata->size, typetag_size);
        if (ret < 0)
            return luaL_error(L, "%s", blitpack_strerr(ret));
        size += ret;
        uint32_t iterator = 0;
        int membercount = 0;

        for (size_t i = 0; i < typetag_size; i++)
        {
            if (lua_next(L, -1) == 0)
                break;
            if (lua_type(L, -2) != LUA_TNUMBER)
                return luaL_error(L, "Packet values cannot be key value pairs (at index %d)", (int)i);
            // int value_type = lua_type(L, -argc + i);
            // switch (typetag[i])
            // {
            // case BLITPACK_TNIL:
            //     ret = blitpack_write_nil(userdata->buffer, userdata->size, &iterator);
            //     break;
            // case BLITPACK_TINF:
            //     ret = blitpack_write_inf(userdata->buffer, userdata->size, &iterator);
            //     break;
            // case BLITPACK_TBOOL:
            //     if (value_type == LUA_TTABLE)
            //     {
            //         size_t array_size = blitpack_lua_table_len_(L, -1);
            //         while (lua_next(L, t) != 0) {
            //             /* uses 'key' (at index -2) and 'value' (at index -1) */
            //             printf("%s - %s\n",
            //                    lua_typename(L, lua_type(L, -2)),
            //                    lua_typename(L, lua_type(L, -1)));
            //             /* removes 'value'; keeps 'key' for next iteration */
            //             lua_pop(L, 1);
            //         }

            //     }
            //     else
            //         ret = blitpack_write_bool(userdata->buffer, userdata->size, &iterator);
            //     break;
            // }
            // if (ret < 0)
            //     return luaL_error(L, "%s", blitpack_strerr(ret));
        }
    }
    else if (lua_gettop(L) == 2)
    {
        luaL_checktype(L, 2, LUA_TTABLE);
        // int64_t ret = blitpack_init(userdata->buffer, userdata->size, argc);
        // if (ret < 0)
        // {
        //     luaL_error(L, "%s", blitpack_strerr(ret));
        //     return 0; // Actually, error will longjmp
        // }
    }
    else
        return luaL_error(L, "Expected 1 or 2 arguments, not %d.", lua_gettop(L) - 1);
    lua_pushinteger(L, size);
    return 1;
}

static int blitpack_lua_buffer_new_(
    lua_State *L
)
{
    lua_Integer size = luaL_checkinteger(L, 1);
    if (size < 0)
    {
        // TODO: error
    }
    struct blitpack_lua_buffer *userdata = lua_newuserdata(L, size + sizeof(struct blitpack_lua_buffer));
    userdata->size = size;
    luaL_newmetatable(L, "buffer");
    lua_pushcfunction(L, blitpack_lua_buffer_length_);
    lua_setfield(L, -2, "__len");
    lua_newtable(L);
    lua_pushcfunction(L, blitpack_lua_buffer_write_);
    lua_setfield(L, -2, "write");
    lua_pushinteger(L, 123);
    lua_setfield(L, -2, "abc");
    lua_setfield(L, -2, "__index");
    lua_setmetatable(L, -2);
    return 1;
}

int luaopen_blitpack(
    lua_State *L
)
{
    lua_newtable(L);
    lua_pushcfunction(L, blitpack_lua_buffer_new_);
    lua_setfield(L, -2, "buffer");
    return 1;
}
