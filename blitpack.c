#include "blitpack.h"

#include <string.h>
#include <stdio.h>
#include <assert.h>

#define BLITPACK_SIZE_(length_, aligned_, size_, extra_) \
     do \
     { \
         (aligned_) = BLITPACK_ALIGNED((length_) extra_); \
         if ((size_) > BLITPACK_SIZE_MAX - (aligned_)) return BLITPACK_EOVERFLOW; \
         (size_) += (aligned_); \
     } while (0)

#if BLITPACK_ZEROPAD == 1
#define BLITPACK_ZEROPAD_(ptr_, size_) memset((ptr_), 0, (size_))
#else
#define BLITPACK_ZEROPAD_(ptr_, size_) do { } while (0)
#endif

#define BLITPACK_DEFINE_WRITE_NONPAYLOAD_(name, blittype) \
    int64_t name( \
        void *buffer, \
        int64_t buffer_size, \
        uint32_t *iterator \
    ) \
    { \
        if (buffer != NULL) \
        { \
            uint32_t num_members; \
            uint32_t typetag_offset; \
            int64_t ret = blitpack_read_head_(buffer, buffer_size, NULL, &typetag_offset, &num_members, NULL); \
            if (ret < 0) \
                return ret; \
            if (iterator == NULL || *iterator >= num_members) \
                return BLITPACK_EITERATOR; \
            ((char *)buffer)[typetag_offset + *iterator] = blittype; \
            (*iterator)++; \
        } \
        return 0; \
    }

#define BLITPACK_DEFINE_WRITE_SCALAR_(name, ctype, blittype, extra_zero) \
    int64_t name( \
        void *buffer, \
        int64_t buffer_size, \
        uint32_t *iterator, \
        const ctype *value, \
        uint32_t length \
    ) \
    { \
        int64_t aligned; \
        int64_t value_size = length; \
        value_size *= sizeof(ctype); \
        aligned = BLITPACK_ALIGNED(value_size); \
        if (aligned > BLITPACK_SIZE_MAX) return BLITPACK_ESIZE; \
        if (buffer != NULL) \
        { \
            uint32_t packet_size, num_members, typetag_offset, member_sizes_offset; \
            int64_t ret = blitpack_read_head_(buffer, buffer_size, &packet_size, &typetag_offset, &num_members, &member_sizes_offset); \
            if (ret < 0) return ret; \
            if (packet_size > BLITPACK_SIZE_MAX - aligned) return BLITPACK_ESIZE; \
            if (buffer_size < packet_size + aligned) return BLITPACK_EOVERRUN; \
            if (iterator == NULL || *iterator >= num_members) return BLITPACK_EITERATOR; \
            if (value != NULL) \
                memcpy((char *)buffer + packet_size, value, value_size); \
            else \
                memset((char *)buffer + packet_size, 0, value_size); \
            BLITPACK_ZEROPAD_((char *)buffer + packet_size + value_size, aligned - value_size); \
            packet_size += aligned; \
            memcpy((uint32_t *)buffer + 1, &packet_size, sizeof(uint32_t)); \
            ((char *)buffer)[typetag_offset + *iterator] = blittype; \
            memcpy((char *)buffer + member_sizes_offset + sizeof(uint32_t) * (*iterator), (uint32_t[]){value_size}, sizeof(uint32_t)); \
            (*iterator)++; \
        } \
        return aligned; \
    }

#define BLITPACK_DEFINE_READ_NOPAYLOAD_(name, blittype) \
    int64_t name( \
        const void *buffer, \
        int64_t buffer_size, \
        uint32_t *iterator \
    ) \
    { \
        uint32_t num_members, typetag_offset; \
        int64_t ret = blitpack_read_head_(buffer, buffer_size, NULL, &typetag_offset, &num_members, NULL); \
        if (ret < 0) return ret; \
        if (iterator == NULL || *iterator >= num_members) return BLITPACK_EITERATOR; \
        const char *typetag = (const char *)buffer + typetag_offset; \
        assert(typetag[*iterator] == BLITPACK_TNIL || typetag[*iterator] == BLITPACK_TINF); \
        if (typetag[*iterator] != blittype) return BLITPACK_ECONV; \
        (*iterator)++; \
        return 0; \
    }

#define BLITPACK_DEFINE_READ_SCALAR_(name, ctype, blittype) \
    int64_t name( \
        const void *buffer, \
        int64_t buffer_size, \
        uint32_t *iterator, \
        uint32_t *offset, \
        ctype *value, \
        uint32_t value_n, \
        uint32_t *length \
    ) \
    { \
        uint32_t packet_size, num_members, typetag_offset, member_sizes_offset; \
        int64_t ret = blitpack_read_head_(buffer, buffer_size, &packet_size, &typetag_offset, &num_members, &member_sizes_offset); \
        if (ret < 0) return ret; \
        if (buffer_size < packet_size) return BLITPACK_EOVERRUN; \
        if (iterator == NULL || *iterator >= num_members) return BLITPACK_EITERATOR; \
        if (offset == NULL) return BLITPACK_EOFFSET; \
        int64_t type_size; \
        const char *typetag = (const char *)buffer + typetag_offset; \
        BLITPACK_VALIDATE_TYPE_SWITCH_(typetag[*iterator], type_size); \
        if (type_size < 0) return BLITPACK_ETYPE; \
        if (typetag[*iterator] != blittype) return BLITPACK_ECONV; \
        uint32_t value_size; \
        memcpy(&value_size, (const char *)buffer + member_sizes_offset + sizeof(uint32_t) * (*iterator), sizeof(uint32_t)); \
        if (value_size > BLITPACK_SIZE_MAX) return BLITPACK_EVALUE; \
        if (value_size > 0 && type_size > 0 && !BLITPACK_ISALIGNEDW(value_size, type_size)) return BLITPACK_EALIGN; \
        int64_t totsize = BLITPACK_ALIGNED(value_size); \
        if (*offset == 0) \
            *offset += member_sizes_offset + sizeof(uint32_t) * num_members; \
        else if (*offset < member_sizes_offset + sizeof(uint32_t) * num_members || *offset >= (int64_t)packet_size || !BLITPACK_ISALIGNED(*offset)) \
            return BLITPACK_EOFFSET; \
        if (buffer_size < *offset + totsize) return BLITPACK_EOVERRUN; \
        int64_t max_size = value_n * type_size; \
        int64_t copy_size = max_size < (int64_t)value_size ? max_size : value_size; \
        if (value != NULL && copy_size > 0) \
            memcpy(value, (const char *)buffer + *offset + sizeof(uint32_t), copy_size); \
        if (length != NULL) *length = value_size / sizeof(ctype); \
        (*iterator)++; \
        *offset += totsize; \
        return totsize; \
    }

#define BLITPACK_VALIDATE_TYPE_SWITCH_(type_, lvalue_) \
    do \
    { \
    switch ((type_)) \
    { \
    case BLITPACK_TNIL: \
    case BLITPACK_TINF: \
        (lvalue_) = 0; break; \
    case BLITPACK_TBOOL: \
        (lvalue_) = sizeof(bool); break; \
    case BLITPACK_TINT8: \
    case BLITPACK_TUINT8: \
        (lvalue_) = sizeof(uint8_t); break; \
    case BLITPACK_TINT16: \
    case BLITPACK_TUINT16: \
        (lvalue_) = sizeof(uint16_t); break; \
    case BLITPACK_TINT32: \
    case BLITPACK_TUINT32: \
        (lvalue_) = sizeof(uint32_t); break; \
    case BLITPACK_TINT64:  \
    case BLITPACK_TUINT64: \
        (lvalue_) = sizeof(uint64_t); break; \
    case BLITPACK_TFLOAT: (lvalue_) = sizeof(float); break; \
    case BLITPACK_TDOUBLE: (lvalue_) = sizeof(double); break; \
    case BLITPACK_TSTRING: (lvalue_) = sizeof(char); break; \
    case BLITPACK_TMAP: (lvalue_) = sizeof(uint32_t); break; \
    case BLITPACK_TPACKET: (lvalue_) = sizeof(uint32_t); break; \
    default: (lvalue_) = BLITPACK_ETYPE; break; \
    } \
    } while (0)

inline static int64_t blitpack_strnlen_(
    const char *s,
    int64_t n
)
{
    int64_t len = 0;
    if (s != NULL)
        while (len < n && s[len] != 0)
            len++;
    return len;
}

inline static int64_t blitpack_size_(
    uint32_t length,
    uint32_t type_size,
    int64_t *payload_size
)
{
    uint64_t size = length;
    size *= type_size;
    if (size > BLITPACK_SIZE_MAX)
        return BLITPACK_ESIZE;
    *payload_size = size;
    return BLITPACK_ALIGNED(size);
}

inline static int64_t blitpack_read_head_(
    const void *buffer,
    int64_t buffer_size,
    uint32_t *packet_size,
    uint32_t *typetag_offset,
    uint32_t *num_members,
    uint32_t *member_sizes_offset
)
{
    uint32_t head[4];
    if (buffer_size < (int64_t)sizeof(head)) return BLITPACK_EOVERRUN;
    memcpy(head, buffer, sizeof(head));
    if (head[0] == BLITPACK_HEAD_SWAPPED) return BLITPACK_EENDIAN;
    if (head[0] != BLITPACK_HEAD) return BLITPACK_EPACKET;
    if (head[1] > BLITPACK_SIZE_MAX || head[1] < sizeof(head) || !BLITPACK_ISALIGNED(head[1])) return BLITPACK_EPACKET;
    if (head[2] >= BLITPACK_SIZE_MAX || head[2] >= head[1] - sizeof(head)) return BLITPACK_EPACKET;
    int64_t size = sizeof(uint32_t[3]);
    size += BLITPACK_ALIGNED(head[2] + 1);
    uint32_t szofs = size;
    size += sizeof(uint32_t) * head[2];
    if ((int64_t)head[1] < size) return BLITPACK_EPACKET;
    if (buffer_size < size) return BLITPACK_EOVERRUN;
    if (packet_size != NULL)
        *packet_size = head[1];
    if (typetag_offset != NULL)
        *typetag_offset = sizeof(uint32_t[3]);
    if (num_members != NULL)
        *num_members = head[2];
    if (member_sizes_offset != NULL)
        *member_sizes_offset = szofs;
    return size;
}

const char *blitpack_type_name(
    int type
)
{
    switch (type)
    {
    case BLITPACK_TNIL:
        return "nil";
    case BLITPACK_TINF:
        return "inf";
    case BLITPACK_TBOOL:
        return "bool";
    case BLITPACK_TINT8:
        return "int8";
    case BLITPACK_TUINT8:
        return "uint8";
    case BLITPACK_TINT16:
        return "int16";
    case BLITPACK_TUINT16:
        return "uint16";
    case BLITPACK_TINT32:
        return "int32";
    case BLITPACK_TUINT32:
        return "uint32";
    case BLITPACK_TINT64:
        return "int64";
    case BLITPACK_TUINT64:
        return "uint64";
    case BLITPACK_TFLOAT:
        return "float";
    case BLITPACK_TDOUBLE:
        return "double";
    case BLITPACK_TSTRING:
        return "string";
    case BLITPACK_TMAP:
        return "map";
    case BLITPACK_TPACKET:
        return "packet";
    }
    return "";
}

const char *blitpack_strerr(
    int64_t error
)
{
    switch (error)
    {
    case BLITPACK_EOVERRUN:
        return "Buffer overrun";
    case BLITPACK_EOVERFLOW:
        return "Integer overflow";
    case BLITPACK_EPACKET:
        return "Invalid packet";
    case BLITPACK_EENDIAN:
        return "Wrong endian";
    case BLITPACK_EITERATOR:
        return "End of iterator";
    case BLITPACK_EINVAL:
        return "Invalid argument";
    case BLITPACK_ETYPE:
        return "Invalid type";
    case BLITPACK_ECONV:
        return "Invalid type conversion";
    case BLITPACK_EOFFSET:
        return "Invalid offset";
    case BLITPACK_EVALUE:
        return "Invalid value";
    case BLITPACK_EALIGN:
        return "Misaligned size";
    }
    return "Unknown error";
}

int64_t blitpack_read_head(
    const void *buffer,
    int64_t buffer_size,
    uint32_t *packet_size,
    uint32_t *typetag_offset,
    uint32_t *num_members,
    uint32_t *member_sizes_offset
)
{
    return blitpack_read_head_(
        buffer, buffer_size, packet_size,
        typetag_offset,
        num_members,
        member_sizes_offset
    );
}

int64_t blitpack_read_size(
    const void *buffer,
    int64_t buffer_size
)
{
    uint32_t size;
    int64_t ret = blitpack_read_head_(buffer, buffer_size, &size, NULL, NULL, NULL);
    if (ret >= 0)
        ret = size;
    return ret;
}

const char *blitpack_read_typetag(
    const void *buffer,
    int64_t buffer_size,
    uint32_t *length
)
{
    uint32_t tt;
    int64_t ret = blitpack_read_head_(buffer, buffer_size, NULL, &tt, length, NULL);
    if (ret < 0)
        return NULL;
    return (const char *)buffer + tt;
}

int64_t blitpack_init(
    void *buffer,
    int64_t buffer_size,
    uint32_t length
)
{
    int64_t aligned, size;
    if (length >= BLITPACK_SIZE_MAX)
        return BLITPACK_ESIZE;
    aligned = BLITPACK_ALIGNED(length + 1);
    size = sizeof(uint32_t[3]);
    size += aligned;
    size += sizeof(uint32_t) * length;
    if (size >= BLITPACK_SIZE_MAX)
        return BLITPACK_ESIZE;
    if (buffer != NULL)
    {
        uint32_t *ptr = buffer;
        if (buffer_size < size)
            return BLITPACK_EOVERRUN;
        memcpy(ptr++, (uint32_t[]){BLITPACK_HEAD}, sizeof(uint32_t));
        memcpy(ptr++, (uint32_t[]){size}, sizeof(uint32_t));
        memcpy(ptr++, (uint32_t[]){length}, sizeof(uint32_t));
        memset(ptr, 0, size - sizeof(uint32_t[3]));
    }
    return size;
}

BLITPACK_DEFINE_WRITE_NONPAYLOAD_(blitpack_write_nil, BLITPACK_TNIL)
BLITPACK_DEFINE_WRITE_NONPAYLOAD_(blitpack_write_inf, BLITPACK_TINF)
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_bool, bool, BLITPACK_TBOOL, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_int8, int8_t, BLITPACK_TINT8, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_uint8, uint8_t, BLITPACK_TUINT8, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_int16, int16_t, BLITPACK_TINT16, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_uint16, uint16_t, BLITPACK_TUINT16, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_int32, int32_t, BLITPACK_TINT32, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_uint32, uint32_t, BLITPACK_TUINT32, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_int64, int64_t, BLITPACK_TINT64, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_uint64, uint64_t, BLITPACK_TUINT64, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_float, float, BLITPACK_TFLOAT, )
BLITPACK_DEFINE_WRITE_SCALAR_(blitpack_write_double, double, BLITPACK_TDOUBLE, )

int64_t blitpack_write_string(
    void *buffer,
    int64_t buffer_size,
    uint32_t *iterator,
    const char *s,
    uint32_t max_length
)
{
    int64_t aligned;
    uint32_t length = 0;
    if (s != NULL)
        while (length < max_length && s[length] != 0)
            length++;
    int64_t value_size = length;
    if (length >= BLITPACK_SIZE_MAX) return BLITPACK_ESIZE;
    aligned = BLITPACK_ALIGNED(value_size + 1);
    if (aligned > BLITPACK_SIZE_MAX) return BLITPACK_ESIZE;
    if (buffer != NULL)
    {
        uint32_t packet_size, num_members, typetag_offset, member_sizes_offset;
        int64_t ret = blitpack_read_head_(buffer, buffer_size, &packet_size, &typetag_offset, &num_members, &member_sizes_offset);
        if (ret < 0) return ret;
        if (packet_size > BLITPACK_SIZE_MAX - aligned) return BLITPACK_ESIZE;
        if (buffer_size < packet_size + aligned) return BLITPACK_EOVERRUN;
        if (iterator == NULL || *iterator >= num_members) return BLITPACK_EITERATOR;
        if (s != NULL && value_size > 0)
            memcpy((char *)buffer + packet_size, s, value_size);
        BLITPACK_ZEROPAD_((char *)buffer + packet_size + value_size, aligned - value_size);
        packet_size += aligned;
        memcpy((uint32_t *)buffer + 1, &packet_size, sizeof(uint32_t));
        ((char *)buffer)[typetag_offset + *iterator] = BLITPACK_TSTRING;
        memcpy((char *)buffer + member_sizes_offset + sizeof(uint32_t) * (*iterator), (uint32_t[]){value_size}, sizeof(uint32_t));
        (*iterator)++;
    }
    return aligned;
}

int64_t blitpack_write_packet(
    void *buffer,
    int64_t buffer_size,
    uint32_t *iterator,
    const void *value,
    uint32_t value_n,
    void **packet_ptr
)
{
    uint32_t value_size;
    int64_t ret;
    if (value != NULL)
    {
        ret = blitpack_read_head_(value, value_n, &value_size, NULL, NULL, NULL);
        if (ret < 0) return BLITPACK_EVALUE;
    }
    else
        value_size = value_n;
    if (!BLITPACK_ISALIGNED(value_size) || value_size < sizeof(uint32_t[4])) return BLITPACK_EINVAL;
    if (buffer != NULL)
    {
        uint32_t packet_size, num_members, typetag_offset, member_sizes_offset;
        ret = blitpack_read_head_(buffer, buffer_size, &packet_size, &typetag_offset, &num_members, &member_sizes_offset);
        if (ret < 0) return ret;
        if (packet_size > BLITPACK_SIZE_MAX - value_size) return BLITPACK_ESIZE;
        if (buffer_size < packet_size + value_size) return BLITPACK_EOVERRUN;
        if (iterator == NULL || *iterator >= num_members) return BLITPACK_EITERATOR;
        if (value != NULL)
            memcpy((char *)buffer + packet_size, value, value_size);
        else
            memset((char *)buffer + packet_size, 0, value_size);
        if (packet_ptr != NULL)
            *packet_ptr = (char *)buffer + packet_size;
        packet_size += value_size;
        memcpy((uint32_t *)buffer + 1, &packet_size, sizeof(uint32_t));
        ((char *)buffer)[typetag_offset + *iterator] = BLITPACK_TPACKET;
        memcpy((char *)buffer + member_sizes_offset + sizeof(uint32_t) * (*iterator), (uint32_t[]){value_size}, sizeof(uint32_t));
        (*iterator)++;
    }
    return value_size;
}

BLITPACK_DEFINE_READ_NOPAYLOAD_(blitpack_read_nil, BLITPACK_TNIL)
BLITPACK_DEFINE_READ_NOPAYLOAD_(blitpack_read_inf, BLITPACK_TINF)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_bool, bool, BLITPACK_TBOOL)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_int8, int8_t, BLITPACK_TINT8)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_uint8, uint8_t, BLITPACK_TUINT8)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_int16, int16_t, BLITPACK_TINT16)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_uint16, uint16_t, BLITPACK_TUINT16)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_int32, int32_t, BLITPACK_TINT32)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_uint32, uint32_t, BLITPACK_TUINT32)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_int64, int64_t, BLITPACK_TINT64)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_uint64, uint64_t, BLITPACK_TUINT64)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_float, float, BLITPACK_TFLOAT)
BLITPACK_DEFINE_READ_SCALAR_(blitpack_read_double, double, BLITPACK_TDOUBLE)

int64_t blitpack_read_string_ptr(
    const void *buffer,
    int64_t buffer_size,
    uint32_t *iterator,
    uint32_t *offset,
    const char **s,
    uint32_t *length
)
{
    uint32_t packet_size, num_members, typetag_offset, member_sizes_offset;
    int64_t ret = blitpack_read_head_(buffer, buffer_size, &packet_size, &typetag_offset, &num_members, &member_sizes_offset);
    if (ret < 0) return ret;
    if (buffer_size < packet_size) return BLITPACK_EOVERRUN;
    if (iterator == NULL || *iterator >= num_members) return BLITPACK_EITERATOR;
    if (offset == NULL) return BLITPACK_EOFFSET;
    const char *typetag = (const char *)buffer + typetag_offset;
    if (typetag[*iterator] != BLITPACK_TSTRING) return BLITPACK_ECONV;
    uint32_t value_size;
    memcpy(&value_size, (const char *)buffer + member_sizes_offset + sizeof(uint32_t) * (*iterator), sizeof(uint32_t));
    if (value_size >= BLITPACK_SIZE_MAX) return BLITPACK_EVALUE;
    if (offset == 0)
        *offset += member_sizes_offset + sizeof(uint32_t) * num_members;
    else if (*offset < member_sizes_offset + sizeof(uint32_t) * num_members || *offset >= (int64_t)packet_size || !BLITPACK_ISALIGNED(*offset))
        return BLITPACK_EOFFSET;
    if (buffer_size < *offset + value_size) return BLITPACK_EOVERRUN;
    if (s != NULL)
        *s = (const char *)buffer + *offset;
    value_size = BLITPACK_ALIGNED(value_size);
    if (length != NULL) *length = value_size;
    (*iterator)++;
    *offset += value_size;
    return value_size;
}

int64_t blitpack_read_string(
    const void *buffer,
    int64_t buffer_size,
    uint32_t *iterator,
    uint32_t *offset,
    char *s,
    uint32_t s_n
)
{
    const char *tmps;
    uint32_t length;
    int64_t ret = blitpack_read_string_ptr(
        buffer,
        buffer_size,
        iterator,
        offset,
        &tmps,
        &length
    );
    if (ret >= 0 && s != NULL && s_n > 0)
    {
        s_n--;
        if (length < s_n)
            length = s_n;
        memcpy(s, tmps, length);
        s[length] = 0;
    }
    return ret;
}

int64_t blitpack_read_packet_ptr(
    const void *buffer,
    int64_t buffer_size,
    uint32_t *iterator,
    uint32_t *offset,
    const void **value,
    uint32_t *size
)
{
    uint32_t packet_size, num_members, typetag_offset, member_sizes_offset;
    int64_t ret = blitpack_read_head_(buffer, buffer_size, &packet_size, &typetag_offset, &num_members, &member_sizes_offset);
    if (ret < 0) return ret;
    if (buffer_size < packet_size) return BLITPACK_EOVERRUN;
    if (iterator == NULL || *iterator >= num_members) return BLITPACK_EITERATOR;
    if (offset == NULL) return BLITPACK_EOFFSET;
    const char *typetag = (const char *)buffer + typetag_offset;
    if (typetag[*iterator] != BLITPACK_TPACKET) return BLITPACK_ECONV; \
    uint32_t value_size;
    memcpy(&value_size, (const char *)buffer + member_sizes_offset + sizeof(uint32_t) * (*iterator), sizeof(uint32_t));
    if (value_size > BLITPACK_SIZE_MAX) return BLITPACK_EVALUE;
    if (value_size > 0 && !BLITPACK_ISALIGNED(value_size)) return BLITPACK_EALIGN;
    if (offset == 0)
        *offset += member_sizes_offset + sizeof(uint32_t) * num_members;
    else if (*offset < member_sizes_offset + sizeof(uint32_t) * num_members || *offset >= (int64_t)packet_size || !BLITPACK_ISALIGNED(*offset))
        return BLITPACK_EOFFSET;
    if (buffer_size < *offset + value_size) return BLITPACK_EOVERRUN;
    if (value != NULL)
        *value = (const char *)buffer + *offset;
    if (size != NULL) *size = value_size;
    (*iterator)++;
    *offset += value_size;
    return value_size;
}

int main(int argc, char *argv[])
{
    for (int i = 0; i < 1000000; i++)
    {
        int64_t retsize;
        uint32_t iterator = 0;
        unsigned char tmps2[2048] = {0};
        retsize = blitpack_init(tmps2, sizeof(tmps2), 1);
        retsize = blitpack_write_bool(tmps2, sizeof(tmps2), &iterator, (bool[]){true, true, true, true}, 4);
        
        unsigned char tmps[2048] = {0};
        iterator = 0;
        retsize = blitpack_init(tmps, sizeof(tmps), 15);
        retsize = blitpack_write_nil(tmps, sizeof(tmps), &iterator);
        retsize = blitpack_write_inf(tmps, sizeof(tmps), &iterator);
        retsize = blitpack_write_bool(tmps, sizeof(tmps), &iterator, (bool[]){true, false, true, false}, 4);
        retsize = blitpack_write_int8(tmps, sizeof(tmps), &iterator, (int8_t[]){1, 2, 3, 4}, 4);
        retsize = blitpack_write_uint8(tmps, sizeof(tmps), &iterator, (uint8_t[]){1, 2, 3, 4}, 4);
        retsize = blitpack_write_int16(tmps, sizeof(tmps), &iterator, (int16_t[]){1, 2, 3, 4}, 4);
        retsize = blitpack_write_uint16(tmps, sizeof(tmps), &iterator, (uint16_t[]){1, 2, 3, 4}, 4);
        retsize = blitpack_write_int32(tmps, sizeof(tmps), &iterator, (int32_t[]){1, 2, 3, 4}, 4);
        retsize = blitpack_write_uint32(tmps, sizeof(tmps), &iterator, (uint32_t[]){1, 2, 3, 4}, 4);
        retsize = blitpack_write_int64(tmps, sizeof(tmps), &iterator, (int64_t[]){1, 2, 3, 4}, 4);
        retsize = blitpack_write_uint64(tmps, sizeof(tmps), &iterator, (uint64_t[]){1, 2, 3, 4}, 4);
        retsize = blitpack_write_float(tmps, sizeof(tmps), &iterator, (float[]){1.1, 2.2, 3.3, 4.4}, 4);
        retsize = blitpack_write_double(tmps, sizeof(tmps), &iterator, (double[]){1.1, 2.2, 3.3, 4.4}, 4);
        retsize = blitpack_write_string(tmps, sizeof(tmps), &iterator, "Hello World!", strlen("Hello World!"));
        retsize = blitpack_write_packet(tmps, sizeof(tmps), &iterator, tmps2, sizeof(tmps2), NULL);
        uint32_t packet_size;
        retsize = blitpack_read_head(tmps, sizeof(tmps), &packet_size, NULL, NULL, NULL);
        // for (uint32_t i = 0; i < packet_size; i++)
        //     printf("%c %u = 0x%02x '%c'\n", (i % 4) ? ' ' : '=', i, tmps[i], (tmps[i] > 32 ? (char)tmps[i] : ' '));

        iterator = 0;
        uint32_t offset = 0, length;
        bool value[128] = {0};
        int64_t rsize;
        rsize = blitpack_read_nil(tmps, sizeof(tmps), &iterator);
        // printf("read nil %ld\n", rsize);
        rsize = blitpack_read_inf(tmps, sizeof(tmps), &iterator);
        // printf("read inf %ld\n", rsize);
        rsize = blitpack_read_bool(tmps, sizeof(tmps), &iterator, &offset, value, 128, &length);
        // printf("read bool %ld %u\n", rsize, length); for (uint32_t j = 0; j < length; j++) printf("value %d\n", value[j]);
        rsize = blitpack_read_int8(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read int8 %ld %u\n", rsize, length);
        rsize = blitpack_read_uint8(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read uint8 %ld %u\n", rsize, length);
        rsize = blitpack_read_int16(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read int16 %ld %u\n", rsize, length);
        rsize = blitpack_read_uint16(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read uint16 %ld %u\n", rsize, length);
        rsize = blitpack_read_int32(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read int32 %ld %u\n", rsize, length);
        rsize = blitpack_read_uint32(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read uint32 %ld %u\n", rsize, length);
        rsize = blitpack_read_int64(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read int64 %ld %u\n", rsize, length);
        rsize = blitpack_read_uint64(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read uint64 %ld %u\n", rsize, length);
        rsize = blitpack_read_float(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read float %ld %u\n", rsize, length);
        rsize = blitpack_read_double(tmps, sizeof(tmps), &iterator, &offset, NULL, 0, &length);
        // printf("read double %ld %u\n", rsize, length);
        const char *s;
        rsize = blitpack_read_string_ptr(tmps, sizeof(tmps), &iterator, &offset, &s, &length);
        // printf("read string %ld %u '%s' (%u)\n", rsize, length, s, length);
        const void *subpack = NULL;
        rsize = blitpack_read_packet_ptr(tmps, sizeof(tmps), &iterator, &offset, &subpack, &length);
        // printf("read packet %ld %u %p\n", rsize, length, subpack);
    }

    return 0;
}
